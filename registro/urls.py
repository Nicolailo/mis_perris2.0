from django.urls import path, include
from . import views
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

from rest_framework import routers 

router = routers.DefaultRouter()
router.register(r'personas',views.PersonaList)

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^apitest/',views.test, name = "test"),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'^registro/crear_perro/$',views.crear_perro,name = "crear_perro"),
    url(r'^registro/crear/$',views.crear,name = "crear"),
    url(r'^$',views.index, name="index"),
    url(r'^registro_persona/$',views.persona_form, name="registro_persona"),
    url(r'^registro_perro/$',views.perro_form, name="registro_perro"),
    url(r'^login/$',views.login, name="login"),
    url(r'^login/iniciar/$',views.login_iniciar,name="iniciar"),
    url(r'^cerrarsesion$',views.cerrar_session,name="cerrar_session"),
    url(r'^listado/$',views.listado,name="listado"),
    url(r'^contrasenia/$',views.contrasenia,name="contrasenia"),
    url(r'^contrasenia/$',views.cambio_contrasenia,name="cambio"),
    url(r'^password/recovery/$',
        auth_views.PasswordResetView.as_view(
            template_name='password_reset_form.html',
            html_email_template_name='password_reset_email.html',
        ),
        name='passwordreset',
    ),
    url(
        r'^password/recovery/(?P<uidb64>[0-9A-Za-z-]+)/'
        r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(
            success_url='/login/',
            post_reset_login=True,
            template_name='password_reset_confirm.html',
            post_reset_login_backend=(
                'django.contrib.auth.backends.AllowAllUsersModelBackend'
            ),
        ),
        name='password_reset_confirm',
    ),
    url(
        r'^password/recovery/done/$',
        auth_views.PasswordResetDoneView.as_view(
            template_name='password_reset_done.html',
        ),
        name='password_reset_done',
    ),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
